import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { AuthPage } from './pages/AuthPage';
import { FindPage } from './pages/FindPage';
import { FavouritesPage } from './pages/FavouritesPage';
import { UsersPage } from './pages/UsersPage';

export const useRoutes = (isAuthenticated) => {
  if (isAuthenticated) {
    return (
      <Switch>
        <Route path='/find' exact>
          <FindPage></FindPage>
        </Route>
        <Route path='/users' exact>
          <UsersPage></UsersPage>
        </Route>
        <Route path='/favourites' exact>
          <FavouritesPage></FavouritesPage>
        </Route>
        <Redirect to='/find'></Redirect>
      </Switch>
    );
  }

  return (
    <Switch>
      <Route path='/' exact>
        <AuthPage></AuthPage>
      </Route>
      <Redirect to='/'></Redirect>
    </Switch>
  );
};
