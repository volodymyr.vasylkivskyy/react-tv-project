import React, { useState, useContext, useCallback, useEffect } from 'react';
import { Loader } from '../components/Loader';
import { AuthContext } from '../context/AuthContext';
import { useHttp } from '../hooks/http.hook';

export const FavouritesPage = () => {
  const [favors, setFavors] = useState([]);
  const { loading, request } = useHttp();
  const { token } = useContext(AuthContext);

  const fetchFavourites = useCallback(async () => {
    try {
      const fetched = await request('/api/add', 'GET', null, {
        Authorization: `Bearer ${token}`,
      });
      setFavors(fetched);
    } catch (e) {}
  }, [token, request]);

  const deleteFavourite = (item) => {
    console.log(item);
  };

  useEffect(() => {
    fetchFavourites();
  }, [fetchFavourites]);

  if (loading) {
    return <Loader></Loader>;
  }

  return (
    <div>
      {!loading && (
        <div>
          {favors.map((item) => {
            return (
              <div
                style={{
                  display: 'inline-block',
                  margin: '10px',
                  border: '1px solid black',
                }}
              >
                <img src={item.image}></img>
                <p>{item.name}</p>
                <button onClick={() => deleteFavourite(item)}>Видалити</button>
              </div>
            );
          })}
        </div>
      )}
    </div>
  );
};
