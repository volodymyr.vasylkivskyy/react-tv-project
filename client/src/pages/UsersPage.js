import React, { useState, useContext, useCallback, useEffect } from 'react';
import { Loader } from '../components/Loader';
import { AuthContext } from '../context/AuthContext';
import { useHttp } from '../hooks/http.hook';

export const UsersPage = () => {
  const [users, setUsers] = useState([]);
  const { loading, request } = useHttp();
  const { token } = useContext(AuthContext);

  const fetchUsers = useCallback(async () => {
    try {
      const fetched = await request('/api/users/get', 'GET', null, {
        Authorization: `Bearer ${token}`,
      });
      setUsers(fetched);
    } catch (e) {}
  }, [token, request]);

  useEffect(() => {
    fetchUsers();
  }, [fetchUsers]);

  if (loading) {
    return <Loader></Loader>;
  }

  return (
    <div>
      {!loading && (
        <div>
          {users.map((item) => {
            return (
              <div
                style={{
                  display: 'inline-block',
                  margin: '10px',
                  border: '1px solid black',
                }}
              >
                <p>{item.email}</p>
                <button>Добавити в друзі</button>
              </div>
            );
          })}
        </div>
      )}
    </div>
  );
};
