import React, { useEffect, useState, useContext, useCallback } from 'react';
import { useHttp } from '../hooks/http.hook';
import { AuthContext } from '../context/AuthContext';
import { useHistory } from 'react-router-dom';

export const FindPage = () => {
  const sth = 'http://placehold.it/350x150/green';
  const history = useHistory();
  const auth = useContext(AuthContext);
  const { request } = useHttp();
  const [data, setData] = useState('');
  const [images, setImages] = useState([]);

  useEffect(() => {
    window.M.updateTextFields();
  }, []);

  const inputHandler = async (event) => {
    const data2 = await request(
      `http://api.tvmaze.com/search/shows?q=${event.target.value}`,
      'GET'
    );
    setData(data2);
  };
  const showResult = () => {
    const data_images = data.map((item) => {
      if (item.show.image != null) {
        return {
          name: item.show.name,
          image: item.show.image.medium,
          score: item.score,
        };
      }
      return { name: item.show.name, image: 'http://placehold.it/200x50' };
    });
    if (data_images.length == 0) {
      setImages(['Результатів не знайдено']);
    } else {
      setImages(data_images);
    }
  };

  const addFavourite = async (item) => {
    try {
      const data = await request(
        '/api/add/favor',
        'POST',
        { data: item },
        { Authorization: `Bearer ${auth.token}` }
      );
    } catch {
      console.log('ERROR');
    }
  };
  return (
    <div className='row'>
      <div className='col s8 offset-s2' style={{ paddingTop: '2rem' }}>
        <div className='input-field'>
          <div className='input-field'>
            <input
              placeholder='Введіть назву фільму'
              id='link'
              type='text'
              className='yellow-input'
              onChange={inputHandler}
            />
            <label htmlFor='link'>Введіть назву фільму</label>
            <div>
              <button onClick={showResult}>Show</button>
            </div>
            <div>
              {images.map((item, index) => {
                return (
                  <div
                    style={{
                      display: 'inline-block',
                      margin: '10px',
                      border: '1px solid black',
                    }}
                  >
                    <img src={item.image}></img>
                    <p>{item.name || 'Результатів не знайдено'}</p>
                    <p>Score: {item.score || 'Результатів не знайдено'}</p>
                    <button onClick={() => addFavourite(item)}>
                      Добавити в улюблене
                    </button>
                    <br></br>
                    <br></br>
                  </div>
                );
              })}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
