import React, { useContext } from 'react';
import { NavLink, useHistory } from 'react-router-dom';
import { AuthContext } from '../context/AuthContext';

export const Navbar = () => {
  const history = useHistory();
  const auth = useContext(AuthContext);
  const logoutHandler = (event) => {
    event.preventDefault();
    auth.logout();
    history.push('/');
  };

  return (
    <nav>
      <div className='nav-wrapper blue darken-1' style={{ padding: '0 2rem' }}>
        <span className='brand-logo'>TV maze</span>
        <ul id='nav-mobile' class='right hide-on-med-and-down'>
          <li>
            <NavLink to='/find'>Пошук</NavLink>
          </li>
          <li>
            <NavLink to='/favourites'>Улюблене</NavLink>
          </li>
          <li>
            <NavLink to='/users'>Користувачі</NavLink>
          </li>
          <li>
            <a href='/' onClick={logoutHandler}>
              Вийти
            </a>
          </li>
        </ul>
      </div>
    </nav>
  );
};
