const { Router } = require('express');
const Favourites = require('../models/Favourites');
const auth = require('../middleware/auth.middleware');
const router = Router();
const User = require('../models/User');

router.get('/get', auth, async (req, res) => {
  try {
    const users = await User.find({});
    res.json(users);
  } catch (e) {
    res.status(500).json({ message: 'Щось пішло не так' });
  }
});

module.exports = router;
