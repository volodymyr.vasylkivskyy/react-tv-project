const { Router } = require('express');
const Favourites = require('../models/Favourites');
const auth = require('../middleware/auth.middleware');
const router = Router();

router.post('/favor', auth, async (req, res) => {
  try {
    const { data } = req.body;
    const favourites = new Favourites({
      name: data.name,
      image: data.image,
      owner: req.user.userId,
    });
    await favourites.save();
    res.status(201).json({ message: 'this is working' });
  } catch (e) {
    res.status(500).json({ message: 'Щось пішло не так' });
  }
});

router.get('/', auth, async (req, res) => {
  try {
    const favors = await Favourites.find({ owner: req.user.userId });
    res.json(favors);
  } catch (e) {
    res
      .status(500)
      .json({ message: 'Щось пішло не так, улюблених не знайдено' });
  }
});

module.exports = router;
